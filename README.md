# Probability Monads


## Description
Analyzing the creation, manipulation and modeling of standard probability
distributions like the Bayesian Network, Monty Hall Problem and Markov Decision Processes through novel computation bases using Haskell. The idea of Probability distributions as monads is largely used in supply chain optimization. 


## Project status
Project currently under development
