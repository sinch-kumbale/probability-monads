module ProbabilityMonads where

import           Control.Applicative
import qualified Data.Map            as M
import           Text.Printf


type Prob = Double

newtype Dist a = Dist [(a, Prob)]


unpackDist :: Dist a -> [(a, Prob)]
unpackDist (Dist xs) = xs


squishD :: (Ord a) => Dist a -> Dist a
squishD (Dist xs) = Dist $ M.toList $ M.fromListWith (+) xs


sumP :: [(a, Prob)] -> Prob
sumP = sum . map snd

normP :: [(a, Prob)] -> [(a, Prob)]
normP xs = [(x, p / q) | let q = sumP xs, (x, p) <- xs]

instance (Show a, Ord a) => Show (Dist a) where
  show d = concatMap showRow $ (unpackDist . squishD) d
    where
      showRow (elem, prob) = padded elem ++ " | " ++ printf "%.4f" prob ++ "\n"
      padded elem = replicate (maxElemLen - (length . show) elem) ' ' ++ show elem
      maxElemLen = maximum $ map (length . show . fst) (unpackDist d)

type Event a = a -> Bool

evalD :: Event a -> Dist a -> Prob
evalD p = sumP . filter (p . fst) . unpackDist


uniform :: [a] -> Dist a
uniform xs = Dist . normP $ map (,1.0) xs


die :: Int -> Dist Int
die n = uniform [1 .. n]


coin :: Prob -> a -> a -> Dist a
coin f x y
  | f < 0.0 || f > 1.0 = error "f must be between 0 and 1"
  | otherwise = Dist [(x, f), (y, 1 - f)]

instance Functor Dist where
  fmap f (Dist xs) = Dist $ [(f x, p) | (x, p) <- xs]

instance Applicative Dist where
  pure x = Dist [(x, 1.0)]

  (Dist fs) <*> (Dist xs) = Dist $ do
    (x, px) <- xs
    (f, pf) <- fs
    return (f x, px * pf)

-- | Binomial distribution with n experiments and success probability p
binom :: Int -> Prob -> Dist Int
binom n p = foldl1 (\x y -> squishD (liftA2 (+) x y)) $ replicate n (coin p 1 0)

instance Monad Dist where
  -- (>>=) :: Dist a -> (a -> Dist b) -> Dist b
  (Dist xs) >>= f = Dist $ do
    (x, p) <- xs
    (y, p') <- unpackDist (f x)
    return (y, p * p')

-- Conditioning on an Event
-- ========================================

-- | Condition a distribution on an event
condD :: (a -> Bool) -> Dist a -> Dist a
condD f (Dist xs) = Dist . normP $ filter (f . fst) xs

bayesMedicalTest = evalD fst . condD snd $ do
  hasDisease <- coin 0.01 True False
  testPositive <-
    if hasDisease
      then coin 0.95 True False
      else coin 0.05 True False
  return (hasDisease, testPositive)
